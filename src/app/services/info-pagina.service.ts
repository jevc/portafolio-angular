import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { InfoPagina } from '../interfaces/info-pagina.interface';

@Injectable({
  providedIn: 'root'
})
export class InfoPaginaService {

  info: InfoPagina = {};
  cargada = false;
  equipo: any[] = [];

  constructor(private http :  HttpClient) { 
      this.cargarInfo();  
      this.cargarEquipo();
  }

  private cargarInfo() {

    // carga el archivo Json de Configuración de la página
    this.http.get('assets/data/appConfig.json')
      .subscribe( (resp:InfoPagina) => {

        this.cargada = true;
        this.info = resp;
        
      })

  };

  private cargarEquipo() {

    // carga el archivo Json de Configuración de la página
    this.http.get('https://portalfolio-angular.firebaseio.com/equipo.json')
      .subscribe( (resp: any[]) => {

        this.equipo = resp;
                
      })

  };

}
